package com.fursa.ncov19.demo.controller;

import com.fursa.ncov19.demo.CoronavirusApplication;
import com.fursa.ncov19.demo.service.CoronavirusDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    final CoronavirusDataService service;

    @Autowired
    public HomeController(CoronavirusDataService service) {
        this.service = service;
    }

    @GetMapping(path = "/")
    public String home(Model model) {
        model.addAttribute("locationStats", service.getAllStats());
        model.addAttribute("totalNewCases", service.getTotalNewCases());
        model.addAttribute("totalCases", service.getTotalReportedCases());
        return "home";
    }
}
